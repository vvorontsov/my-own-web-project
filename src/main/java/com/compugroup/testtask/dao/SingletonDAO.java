package com.compugroup.testtask.dao;

import com.compugroup.testtask.dao.element.UserDAO;

/**
 * Class provides singletons of each DAO.
 */
public class SingletonDAO {

    private static UserDAO userDAO;
    private SingletonDAO() {}

    public static UserDAO getUserDAO() {
        if (userDAO == null) {
            userDAO = new UserDAO();
        }
        return userDAO;
    }
}
