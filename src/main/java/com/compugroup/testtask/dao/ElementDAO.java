package com.compugroup.testtask.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * Generic class - implementation of CRUD interface.
 */
public class ElementDAO<E> implements OperationsCRUD<E> {

    private Class<E> elementClass;

    public ElementDAO(Class<E> elementClass) {
        this.elementClass = elementClass;
    }

    public void addElement(E element, SessionFactory sessionFactory) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.save(element);
        transaction.commit();
    }

    public void updateElement(E element, SessionFactory sessionFactory) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.update(element);
        transaction.commit();
    }

    public E getElementByID(Integer elementId, SessionFactory sessionFactory) {
        E element = null;
        Session session = sessionFactory.openSession();
        element = session.get(elementClass, elementId);
        return element;
    }

    public List<E> getAllElements(SessionFactory sessionFactory) {
        List<E> elements = new ArrayList<E>();
        Session session = sessionFactory.openSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<E> query = builder.createQuery(elementClass);
        Root<E> root = query.from(elementClass);
        query.select(root);
        elements = session.createQuery(query).getResultList();
        return elements;
    }

    public void deleteElement(E element, SessionFactory sessionFactory) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(element);
        transaction.commit();
    }
}
