package com.compugroup.testtask.dao.element.functionality;

import com.compugroup.testtask.dao.OperationsCRUD;
import com.compugroup.testtask.entity.User;
import org.hibernate.SessionFactory;

import java.util.List;

/**
 * Implementation should contain CRUD operations + find user by username + find list of users with specified age.
 * Session is provided.
 */
public interface UserFunctionality extends OperationsCRUD<User> {
    /**
     * @param username name of user.
     * @param sessionFactory session.
     * @return unique user with specified username.
     */
    User findUser(String username, SessionFactory sessionFactory);

    /**
     * @param age required age of user.
     * @param sessionFactory session.
     * @return list of users with specified age.
     */
    List<User> getUsersByAge(Integer age, SessionFactory sessionFactory);
}
