package com.compugroup.testtask.dao.element;

import com.compugroup.testtask.dao.ElementDAO;
import com.compugroup.testtask.dao.element.functionality.UserFunctionality;
import com.compugroup.testtask.entity.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

/**
 * implementation of DAO service for users.
 */
@Repository
@Transactional
public class UserDAO extends ElementDAO<User> implements UserFunctionality {

    public UserDAO() {
        super(User.class);
    }

    /**
     * @param attributeName name of required attribute
     * @param value value of required attribute
     * @param sessionFactory session
     * @param <T> type of required attribute
     * @return result of request to database to get all users with specified value of specified attribute.
     */
    private <T> TypedQuery<User> getUsersByAttribute(String attributeName, T value, SessionFactory sessionFactory) {
        Session session = sessionFactory.openSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<User> crit = builder.createQuery(User.class);
        Root<User> acc = crit.from(User.class);
        crit.where(builder.equal(acc.get(attributeName), value));
        return session.createQuery(crit);
    }

    /**
     * @param username name of user.
     * @param sessionFactory session.
     * @return unique user with specified username.
     */
    @Override
    public User findUser(String username, SessionFactory sessionFactory) {
        TypedQuery<User> queryByUserName = getUsersByAttribute("userName", username, sessionFactory);
        return queryByUserName.getSingleResult();
    }

    /**
     * @param age required age of user.
     * @param sessionFactory session.
     * @return list of users with specified age.
     */
    @Override
    public List<User> getUsersByAge(Integer age, SessionFactory sessionFactory) {
        TypedQuery<User> queryByUserAge = getUsersByAttribute("userAge", age, sessionFactory);
        return queryByUserAge.getResultList();
    }
}