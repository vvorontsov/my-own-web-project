package com.compugroup.testtask.dto;

import com.compugroup.testtask.entity.User;

import java.util.ArrayList;
import java.util.List;

public class UserDto {

    private String userName;

    private String firstName;

    private String lastName;

    private Integer age;

    private String address;

    public UserDto(User user) {
        userName = user.getUserName();
        firstName = user.getUserFirstName();
        lastName = user.getUserLastName();
        age = user.getUserAge();
        address = user.getUserAddress();
    }

    public String getUserName() {
        return userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Integer getAge() {
        return age;
    }

    public String getAddress() {
        return address;
    }

    public static List<UserDto> convertUsersToDto(List<User> users) {
        List<UserDto> usersInfo = new ArrayList<>();
        for (User user: users) {
            usersInfo.add(new UserDto(user));
        }
        return usersInfo;
    }
}
