package com.compugroup.testtask.controller;

import com.compugroup.testtask.dto.UserDto;
import com.compugroup.testtask.entity.User;
import com.compugroup.testtask.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Controller that implements operations with users.
 */
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * @param id id of user in database.
     * @return information about user in json format or HTTP-404 if user not found.
     */
    @GetMapping("/user/{id}")
    public ResponseEntity<UserDto> getUserInfo(@PathVariable(value = "id") Integer id) {
        User user = userService.selectById(id);
        if (user != null) {
            return new ResponseEntity(new UserDto(user), HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * @return list of all users in json format.
     */
    @GetMapping("/users")
    public List<UserDto> getAllUsers() {
        return UserDto.convertUsersToDto(userService.selectAll());
    }

    /**
     * @param age required age of user.
     * @return list of users that have specified value of age in json format.
     */
    @GetMapping("/users/{age}")
    public List<UserDto> getUsersByAge(@PathVariable(value = "age") Integer age) {
        return UserDto.convertUsersToDto(userService.getUserByAge(age));
    }
}
