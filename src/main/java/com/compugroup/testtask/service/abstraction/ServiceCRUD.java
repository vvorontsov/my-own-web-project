package com.compugroup.testtask.service.abstraction;

import java.util.List;

/**
 * Implementation should contain realization of CRUD operations:
 * - create
 * - update
 * - read
 * - delete
 * Each method should use DAO service and provide it with the session.
 * @param <E> entity class
 */
public interface ServiceCRUD<E> {
    void insert(E entity);

    List<E> selectAll();

    E selectById(Integer id);

    void update(E entity);

    void delete(E entity);
}
