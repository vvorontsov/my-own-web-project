package com.compugroup.testtask.service.abstraction;

import com.compugroup.testtask.entity.User;

import java.util.List;

/**
 * /**
 * Service should provide User DAO service with session and connect it with controller.
 */
public interface UserServiceFunctionality extends ServiceCRUD<User>{
    User findUser(String username);

    List<User> getUserByAge(Integer age);
}
