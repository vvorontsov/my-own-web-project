package com.compugroup.testtask.service;

import com.compugroup.testtask.dao.element.functionality.UserFunctionality;
import com.compugroup.testtask.entity.User;
import com.compugroup.testtask.service.abstraction.UserServiceFunctionality;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Implementation service that provides User DAO service with session and connects it with controller.
 */
public class UserService implements UserServiceFunctionality {

    @Autowired
    private UserFunctionality userDAO;

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void insert(User entity) {
        userDAO.addElement(entity, sessionFactory);
    }

    @Override
    public List<User> selectAll() {
        return userDAO.getAllElements(sessionFactory);
    }

    @Override
    public User selectById(Integer id) {
        return userDAO.getElementByID(id, sessionFactory);
    }

    @Override
    public void update(User entity) {
        userDAO.updateElement(entity, sessionFactory);
    }

    @Override
    public void delete(User entity) {
        userDAO.deleteElement(entity, sessionFactory);
    }

    @Override
    public User findUser(String username) {
        return userDAO.findUser(username, sessionFactory);
    }

    @Override
    public List<User> getUserByAge(Integer age) {
        return userDAO.getUsersByAge(age, sessionFactory);
    }
}
