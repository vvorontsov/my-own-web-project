package com.compugroup.testtask.service;

/**
 * Class provides singletons of each service.
 */
public class SingletonService {
    private static UserService userService;

    private SingletonService() {
    }

    public static UserService getUserService() {
        if (userService == null) {
            userService = new UserService();
        }
        return userService;
    }
}
