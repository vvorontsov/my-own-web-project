USE rest_site;
INSERT INTO user (user_name, user_password, user_first_name, user_last_name, user_age, user_address) VALUES
  ('admin', '$2a$04$/P2xiZH.asriSfhX6C9WROBXJ2D3ZrH.ODsYn6O5XvM1lVIeZbZB6', 'Vladyslav', 'Vorontsov', 21,'Auf dem Hellen Weyer 1, 2.21');
INSERT INTO user (user_name, user_password, user_first_name, user_last_name, user_age, user_address) VALUES
  ('user1', '$2a$04$/8ZIvsPyWKDmVXZbH2IcHuQhFLzGSbauESXhYV4IegrgNslwM9/Ly', 'Bill', 'Gates', 62,'Seattle, Washington, USA');
INSERT INTO user (user_name, user_password, user_first_name, user_last_name, user_age, user_address) VALUES
  ('user2', '$2a$04$te0cC4HivljB0D7c0o7/yOdtMvspUeVXsvENslG3xrUAMaEoN9smS', 'Adam', 'Smith', 21, 'Somewhere');
INSERT INTO user (user_name, user_password, user_first_name, user_last_name, user_age, user_address) VALUES
  ('user3', '$2a$04$pCSwQYnih8gOJt9KDikos.fFyw3TPhNCjDkGOxdgXOG1.0cMf5foe', 'Jeremy', 'McCane', 62, 'Somewhere else');
INSERT INTO user (user_name, user_password, user_first_name, user_last_name, user_age, user_address) VALUES
  ('user4', '$2a$04$VfbWR3G20.r5MWs0a2tVyuC12avPXKaTSFyg1DYLD6haEqVbBlHfm', 'Angelina', 'Jolie', 21, 'Somewhere in the US');