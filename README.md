# RESTful web site
---
## To run this web application do the next:
- clone this repository: git clone https://vvorontsov@bitbucket.org/vvorontsov/my-own-web-project.git
- create ***mysql*** database named "rest_site", add user "admin" with password "supersecurepwd". Example of script:
```sql
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema rest_site
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `rest_site` ;
CREATE SCHEMA `rest_site` DEFAULT CHARACTER SET utf8 ;
USE `rest_site`;

DROP USER IF EXISTS 'admin'@'localhost';
CREATE USER 'admin'@'localhost' IDENTIFIED BY 'supersecurepwd';
GRANT ALL PRIVILEGES ON * . * TO 'admin'@'localhost';
FLUSH PRIVILEGES;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
```
- do: ***mvn clean package***;
- go to ***liquibase*** plugin and run command "update";
- run ***Application*** class (src/main/java/com/compugroup/testtask/config/Application.java).
- first of all you need to log in. Without it you can't use other requests because they are protected from 
unauthorized usage. For this do the POST command: ***http://localhost:8080/login?username=admin&password=admin***. If
 entered credentials are wrong, you will get HTTP-401, otherwise: HTTP-200.
- then you allowed to get information about user by id. Do the GET request: ***http://localhost:8080/user/{id}*** 
where {id} is id of required user. Example of responce:
```json
{
    "userName": "user1",
    "firstName": "Bill",
    "lastName": "Gates",
    "age": 62,
    "address": "Seattle, Washington, USA"
}
```
If user with specified id isn't found, you will get HTTP-404.
- furthermore, after authorization you are allowed to get list of users by the value of their age. GET request: 
***http://localhost:8080/users/{age}*** where {age} is required value of age. Example of responce:
```json
[
    {
        "userName": "admin",
        "firstName": "Vladyslav",
        "lastName": "Vorontsov",
        "age": 21,
        "address": "Auf dem Hellen Weyer 1, 2.21"
    },
    {
        "userName": "user2",
        "firstName": "Adam",
        "lastName": "Smith",
        "age": 21,
        "address": "Somewhere"
    },
    {
        "userName": "user4",
        "firstName": "Angelina",
        "lastName": "Jolie",
        "age": 21,
        "address": "Somewhere in the US"
    }
]
```
If there are no users with specified age - returns empty list.
- Advantages of this application:
    - methods "/user/{id}", "/users" and "/users/{age}" are protected from unauthorized usage;
    - returns right HTTP codes (200 and 401) for authorization;
    - users credentials and information are collected in MySQL database;
    - passwords are hashed by BCrypt;
    - database is easy to modify by Liquibase.